docker run --link rabbit-stg:rabbit --env-file=./.envs/stage.env baio/tr-ant-pipe-quotes
docker run --link rabbit-stg:rabbit --link mongo:mongo --env-file=./.envs/stage.env baio/tr-ant-cmr-random


docker-compose -p infrdev -f infr.dev.yml up -d
docker-compose -p infrdev -f infr.dev.yml stop

docker-compose -p infrstg -f infr.stg.yml up -d
docker-compose -p infrstg -f infr.stg.yml stop

docker-compose -f sandbox.1.dev.yml up
docker-compose -f sandbox.3.dev.yml up

docker-compose -f sandbox.1.stg.yml up -d
docker-compose -f sandbox.2.stg.yml up -d
docker-compose -f sandbox.3.stg.yml up -d

docker-compose -f sandbox.1.dev.yml stop


docker-machine create \
-d azure \
--azure-subscription-id="543cfa6a-1869-4b75-b3c0-e75f9c40110b" \
--azure-subscription-cert="azure/mycert.pem" \
--azure-location="North Europe" \
azure-machine-2

eval "$(docker-machine env azure-machine-1)"

azure vm endpoint create azure-machine-2 8083 8083

read logs 
docker logs --tail=100 trhivedocker_cmr7silver7surfer_1

docker logs --tail=100 trhivedocker_pipe7quotes_1

sh ~/dev/tr-hive/tr-hive-docker/tmux/tmux-home.sh
sh ~/lib/tr-hive/tr-hive-docker/tmux/tmux-vvv.sh

docker run -d --privileged --net host cusspvz/autotune

