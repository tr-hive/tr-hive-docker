#!/bin/bash

cd ~/lib

SESSION='tr'

tmux kill-session -t $SESSION
tmux new-session -d -s $SESSION

### APP

tmux new-window -t $SESSION -n azure-1
tmux send-keys 'cd tr-hive/tr-hive-docker' C-m
tmux send-keys 'eval "$(docker-machine env azure-machine-1)"' C-m

tmux new-window -t $SESSION -n azure-2
tmux send-keys 'cd tr-hive/tr-hive-docker' C-m
tmux send-keys 'eval "$(docker-machine env azure-machine-2)"' C-m

tmux new-window -t $SESSION -n azure-3
tmux send-keys 'cd tr-hive/tr-hive-docker' C-m
tmux send-keys 'eval "$(docker-machine env azure-machine-3)"' C-m

tmux new-window -t $SESSION -n docker
tmux send-keys 'cd tr-hive/tr-hive-docker' C-m

tmux new-window -t $SESSION -n libs
tmux send-keys 'cd da-libs' C-m

tmux new-window -t $SESSION -n tr-hive
tmux send-keys 'cd tr-hive' C-m

tmux select-window -t $SESSION:0
 
tmux attach-session -t $SESSION