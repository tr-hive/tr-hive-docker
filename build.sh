#!bin/sh
cd ..

#cd ./tr-ant-cmr-random
#npm update
#docker build -t baio/tr-ant-cmr-random .
#cd ..

cd ./tr-ant-exr-system
npm update
tsc
docker build -t baio/tr-ant-exr-system .
cd ..

cd ./tr-ant-exr-trader-net
npm update
tsc
docker build -t baio/tr-ant-exr-trader-net .
cd ..

cd ./tr-ant-pipe-quotes
npm update
tsc
docker build -t baio/tr-ant-pipe-quotes .
cd ..

cd ./tr-ant-nfr-silver-surfer-random
npm update
tsc
docker build -t baio/tr-ant-nfr-silver-surfer-random .
cd ..

cd ./tr-ant-nfr-silver-surfer
npm update
tsc
docker build -t baio/tr-ant-nfr-silver-surfer .
cd ..

cd ./tr-ant-cmr-silver-surfer
npm update
tsc
docker build -t baio/tr-ant-cmr-silver-surfer .
cd ..